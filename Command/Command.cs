﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Tools.Database.SQLServer
{
    public class Command : IDisposable
    {
        private String _GUID;
        private String _Query;
        private SqlCommand _Command;
        private Connection _Connection;

        private SqlDataReader _DataReader;
        private List<DataColumn> _DataColumns;

        public String GUID { get => _GUID; }
        public Connection Connection { get => _Connection; }

        public Command(String Query, ref Connection Connection)
        {
            this._GUID = this.GenerateGUID();
            this._Query = Query;
            this._Connection = Connection;
            this._Connection.Acquire();
            this._Command = new SqlCommand(Query, this._Connection._SqlConnection);
        }
        public Boolean Parameter(String Parameter, String Value)
        {
            SqlParameter _SqlParameter = null;
            if (Value == null)
            {
                _SqlParameter = new SqlParameter(Parameter, SqlDbType.VarChar)
                {
                    Value = Value
                };
            }
            else if (Value.Length <= 8000)
            {
                _SqlParameter = new SqlParameter(Parameter, SqlDbType.VarChar, Value.Length)
                {
                    Value = Value
                };
            }
            else
            {
                _SqlParameter = new SqlParameter(Parameter, SqlDbType.Text, Value.Length)
                {
                    Value = Value
                };
            }
            return this.Parameter(_SqlParameter);
        }
        public Boolean Parameter(String Parameter, Byte Value)
        {
            SqlParameter _SqlParameter = new SqlParameter(Parameter, SqlDbType.Bit)
            {
                Value = Value
            };
            return this.Parameter(_SqlParameter);
        }
        public Boolean Parameter(String Parameter, Guid Value)
        {
            SqlParameter _SqlParameter = new SqlParameter(Parameter, SqlDbType.UniqueIdentifier)
            {
                Value = Value
            };
            return this.Parameter(_SqlParameter);
        }

        public Boolean Parameter(String Parameter, Boolean Value)
        {
            SqlParameter _SqlParameter = new SqlParameter(Parameter, SqlDbType.Bit)
            {
                Value = Value
            };
            return this.Parameter(_SqlParameter);
        }

        public Boolean Parameter(String Parameter, Int16 Value)
        {
            SqlParameter _SqlParameter = new SqlParameter(Parameter, SqlDbType.Int)
            {
                Value = Value
            };
            return this.Parameter(_SqlParameter);
        }

        public Boolean Parameter(String Parameter, Int32 Value)
        {
            SqlParameter _SqlParameter = new SqlParameter(Parameter, SqlDbType.BigInt)
            {
                Value = Value
            };
            return this.Parameter(_SqlParameter);
        }

        public Boolean Parameter(String Parameter, Byte[] Value)
        {
            SqlParameter _SqlParameter = new SqlParameter(Parameter, SqlDbType.VarBinary)
            {
                Value = Value
            };
            return this.Parameter(_SqlParameter);
        }

        public Boolean Parameter(String Parameter, Double Value)
        {
            SqlParameter _SqlParameter = new SqlParameter(Parameter, SqlDbType.Float)
            {
                Value = Value
            };
            return this.Parameter(_SqlParameter);
        }

        public Boolean Parameter(String Parameter, DateTime Value)
        {
            SqlParameter _SqlParameter = new SqlParameter(Parameter, SqlDbType.DateTime)
            {
                Value = Value
            };
            return this.Parameter(_SqlParameter);
        }

        public Boolean Parameter(String Parameter, Single Value)
        {
            SqlParameter _SqlParameter = new SqlParameter(Parameter, SqlDbType.Decimal)
            {
                Value = Value
            };
            return this.Parameter( _SqlParameter);
        }
        private Boolean Parameter(SqlParameter Parameter)
        {
            try
            {
                this._Connection.Running();
                this._Command.Parameters.Add(Parameter);
                return true;
            }
            catch (ArgumentNullException _ArgumentNullException)
            {
                Tools.Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_ArgumentNullException);
            }
            catch (ArgumentException _ArgumentException)
            {
                Tools.Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_ArgumentException);
            }
            catch (InvalidCastException _InvalidCastException)
            {
                Tools.Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_InvalidCastException);
            }
            catch (Exception _Exception)
            {
                Tools.Log.Exception(MethodBase.GetCurrentMethod(), _Exception);
            }
            return false;
        }
        private Boolean SqlDataReaderInit()
        {
            this._Connection.Running();
            if (this._Connection.Active() && this._DataReader == null)
            {
                try
                {
                    this._DataReader = this._Command.ExecuteReader();
                }
                catch (SqlException _SqlException)
                {
                    this._DataReader = null;
                   Tools.Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_SqlException);
                }
                catch (ObjectDisposedException _ObjectDisposedException)
                {
                    this._DataReader = null;
                    Tools.Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_ObjectDisposedException);
                }
                catch (InvalidCastException _InvalidCastException)
                {
                    this._DataReader = null;
                    Tools.Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_InvalidCastException);
                }
                catch (InvalidOperationException _InvalidOperationException)
                {
                    this._DataReader = null;
                    Tools.Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_InvalidOperationException);
                }
                catch (System.IO.IOException _IOException)
                {
                    this._DataReader = null;
                    Tools.Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_IOException);
                }
                catch (Exception _Exception)
                {
                    this._DataReader = null;
                    Tools.Log.Exception(MethodBase.GetCurrentMethod(), _Exception);
                }
            }
            return (this._DataReader != null ? true : false);
        }
        private Boolean SqlDataReaderRead()
        {
            this._Connection.Running();
            try
            {
                return this._DataReader.Read();
            }
            catch (Exception _Exception)
            {
                Tools.Log.Exception(MethodBase.GetCurrentMethod(), _Exception);
            }
            return false;
            
        }
        private Object SqlDataReaderObject(Int32 Index)
        {
            this._Connection.Running();
            return this._DataReader[Index];
        }
        private Boolean SqlDataReaderFinish()
        {
            this._Connection.Running();
            if (this._DataReader != null)
            {
                this._DataReader.Close();
                this._DataReader = null;
            }
            return true;
        }
        private Boolean SqlDataReaderReset()
        {
            if (this.SqlDataReaderFinish())
            {
                return this.SqlDataReaderInit();
            }
            return false;
        }
        private Boolean DataColumnsInit()
        {
            this._Connection.Usage.CheckIn();
            if (this.SqlDataReaderInit() && this._DataColumns == null)
            {
                DataTable _DataTable = null;
                try
                {
                    _DataTable = this._DataReader.GetSchemaTable();
                }
                catch (InvalidOperationException _InvalidOperationException)
                {
                    _DataTable = null;
                    Tools.Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_InvalidOperationException);
                }
                catch (Exception _Exception)
                {
                    _DataTable = null;
                    Tools.Log.Exception(MethodBase.GetCurrentMethod(), _Exception);
                }
                if (_DataTable != null)
                {
                    this._DataColumns = new List<DataColumn>();
                    foreach (DataRow _DataRow in _DataTable.Rows)
                    {
                        this._DataColumns.Add(new DataColumn((String)_DataRow["ColumnName"], (Type)(_DataRow["DataType"]))
                        {
                            Unique = (Boolean)_DataRow["IsUnique"],
                            ReadOnly = (Boolean)_DataRow["IsReadOnly"],
                            AllowDBNull = (Boolean)_DataRow["AllowDBNull"],
                            AutoIncrement = (Boolean)_DataRow["IsAutoIncrement"],
                        });
                    }
                    _DataTable.Dispose();
                }                
            }
            return (this._DataColumns != null ? true : false);
        }
        private Int32 DataColumnsIndex(String Column)
        {
            if (this.DataColumnsInit())
            {
                for (Int32 _DataColumnLoop = 0; _DataColumnLoop < this._DataColumns.Count; _DataColumnLoop++)
                {
                    if (this._DataColumns[_DataColumnLoop].ColumnName == Column)
                    {
                        return _DataColumnLoop;
                    }
                }
            }
            return -1;
        }
        public DataTable DataTable()
        {
            if (this.DataColumnsInit())
            {
                DataTable _DataTable = new DataTable();
                foreach(DataColumn _DataColumn in this._DataColumns)
                {
                    _DataTable.Columns.Add(_DataColumn);
                }
                while (this.SqlDataReaderRead())
                {
                    DataRow _DataRow = _DataTable.NewRow();
                    for (Int32 _DataColumnsLoop = 0; _DataColumnsLoop < this._DataColumns.Count; _DataColumnsLoop++)
                    {
                        _DataRow[((DataColumn)this._DataColumns[_DataColumnsLoop])] = this.SqlDataReaderObject(_DataColumnsLoop);
                    }
                    _DataTable.Rows.Add(_DataRow);
                }
                return _DataTable;
            }
            return null;
        }
        public Boolean Readable()
        {
            return this.SqlDataReaderReset();
        }
        public Boolean Read()
        {
            return this.SqlDataReaderRead();
        }
        public DataRow DataRow()
        {
            if (this.DataColumnsInit())
            {
                DataTable _DataTable = new DataTable();
                foreach (DataColumn _DataColumn in this._DataColumns)
                {
                    _DataTable.Columns.Add(_DataColumn);
                }
                DataRow _DataRow = _DataTable.NewRow();
                for (Int32 _DataColumnsLoop = 0; _DataColumnsLoop < this._DataColumns.Count; _DataColumnsLoop++)
                {
                    _DataRow[((DataColumn)this._DataColumns[_DataColumnsLoop])] = this.SqlDataReaderObject(_DataColumnsLoop);
                }
                return _DataRow;
            }
            return null;
        }
        public Boolean IsNull(Int32 Index)
        {
            Boolean _IsNull = true;
            if (Index >= 0 && this._DataReader != null && !this._DataReader.IsClosed)
            {
                try
                {
                    _IsNull = this._DataReader.IsDBNull(Index);
                }
                catch
                {
                    _IsNull = true;
                }
            }
            return _IsNull;
        }
        public Boolean IsNull(String Column)
        {
            Int32 _Index = this.DataColumnsIndex(Column);
            return this.IsNull(_Index);
        }
        public Boolean IsString(Int32 Index)
        {
            return (this._DataColumns[Index].DataType.Name == "String" ? true : false);
        }
        public Boolean IsInt32(Int32 Index)
        {
            return (this._DataColumns[Index].DataType.Name == "Int32" ? true : false);
        }
        public Boolean IsInt64(Int32 Index)
        {
            return (this._DataColumns[Index].DataType.Name == "Int64" ? true : false);
        }
        public String GetString(String Column, String Defaul = null)
        {
            this._Connection.Running();
            Int32 _Index = this.DataColumnsIndex(Column);
            String _Value = Defaul;
            if (_Index != -1)
            {
                if (!this.IsNull(_Index))
                {
                    if (this.IsString(_Index))
                        _Value = this._DataReader.GetString(_Index);
                    else if (this.IsInt32(_Index))
                        _Value = this._DataReader.GetInt32(_Index).ToString();
                    else if (this.IsInt64(_Index))
                        _Value = this._DataReader.GetInt64(_Index).ToString();
                }
            }            
            return _Value;
        }
        public Boolean GetBoolean(String Column, Boolean Defaul = false)
        {
            this._Connection.Running();
            Int32 _Index = this.DataColumnsIndex(Column);
            Boolean _Value = Defaul;
            if (! this._DataReader.IsDBNull(_Index))
            {
                try
                {
                    _Value = this._DataReader.GetBoolean(_Index);
                }
                catch
                {
                    _Value = (this._DataReader.GetInt32(_Index) >= 1 ? true : false);
                }
            }
            return _Value;
        }
        public Int16 GetInt16(String Column, Int16 Defaul = 0)
        {
            this._Connection.Running();
            Int32 _Index = this.DataColumnsIndex(Column);
            Int16 _Value = Defaul;
            if (!this._DataReader.IsDBNull(_Index))
            {
                _Value = this._DataReader.GetInt16(_Index);
            }
            return _Value;
        }
        public Int32 GetInt32(String Column, Int32 Defaul = 0)
        {
            this._Connection.Running();
            Int32 _Index = this.DataColumnsIndex(Column);
            Int32 _Value = Defaul;
            if (!this.IsNull(_Index))
            {
                try
                {
                    _Value = this._DataReader.GetInt32(_Index);
                }
                catch { }
            }
            return _Value;
        }
        public Int64 GetInt64(String Column, Int64 Defaul = 0)
        {
            this._Connection.Running();
            Int32 _Index = this.DataColumnsIndex(Column);
            Int64 _Value = Defaul;
            if (!this.IsNull(_Index))
            {
                _Value = this._DataReader.GetInt64(_Index);
            }
            return _Value;
        }
        public Decimal GetDecimal(String Column, Decimal Defaul = 0)
        {
            this._Connection.Running();
            Int32 _Index = this.DataColumnsIndex(Column);
            Decimal _Value = Defaul;
            if (!this.IsNull(_Index))
            {
                _Value = this._DataReader.GetDecimal(_Index);
            }
            return _Value;
        }
        public byte[] GetBytes(String Column, Int32 Size, byte[] Defaul = null)
        {
            this._Connection.Running();
            Int32 _Index = this.DataColumnsIndex(Column);
            byte[] _Value = Defaul;
            if (!this.IsNull(_Index))
            {
                _Value = new byte[Size];
                this._DataReader.GetBytes(_Index, 0, _Value, 0, Size);
            }
            return _Value;
        }
        public Bitmap GetBitmap(String Column, Bitmap Defaul = null)
        {
            this._Connection.Running();
            Int32 _Index = this.DataColumnsIndex(Column);
            Bitmap _Value = Defaul;
            if (!this.IsNull(_Index))
            {
                _Value = (Bitmap)Image.FromStream(new MemoryStream(this._DataReader.GetSqlBinary(_Index).Value));
            }
            return _Value;
        }
        public Image GetImage(String Column, Image Defaul = null)
        {
            this._Connection.Running();
            Int32 _Index = this.DataColumnsIndex(Column);
            Image _Value = Defaul;
            if (! this.IsNull(_Index))
            {
                _Value = Image.FromStream(new MemoryStream(this._DataReader.GetSqlBinary(_Index).Value));
            }
            return _Value;
        }
        public DateTime GetDatetime(String Column)
        {
            this._Connection.Running();
            Int32 _Index = this.DataColumnsIndex(Column);
            DateTime _Value = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            if (!this.IsNull(_Index))
            {
                _Value = this._DataReader.GetDateTime(_Index);
            }
            return _Value;
        }
        public Boolean Execute()
        {
            if (this._Connection.Active())
            {
                try
                {
                    this._Command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception _Exception)
                {
                    Log.Exception(MethodBase.GetCurrentMethod(), _Exception);
                }
            }
            return false;
        }

        public void Dispose()
        {
            if (this._DataReader != null)
            {
                try
                {
                    this._DataReader.Close();
                    this._DataReader = null;
                }
                catch { }
            }
            try
            {
                this._Command.Dispose();
                this._Command = null;
            }
            catch { }
            this._Connection.Release();
        }

        private String GenerateGUID()
        {
            String _String = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
            MD5 _MD5 = System.Security.Cryptography.MD5.Create();
            byte[] _Bytes = System.Text.Encoding.ASCII.GetBytes(_String);
            byte[] _MD5Bytes = _MD5.ComputeHash(_Bytes);
            StringBuilder _StringBuilder = new StringBuilder();
            for (Int16 _MD5BytesLoop = 0; _MD5BytesLoop < _MD5Bytes.Length; _MD5BytesLoop++)
            {
                _StringBuilder.Append(_MD5Bytes[_MD5BytesLoop].ToString("X2"));
            }
            _String = _StringBuilder.ToString();
            return _String.Substring(0, 8) + "-" +
                _String.Substring(8, 4) + "-" +
                _String.Substring(12, 4) + "-" +
                _String.Substring(16, 4) + "-" +
                _String.Substring(20, 12);
        }

    }
}
