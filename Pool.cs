﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Tools.Database.SQLServer
{
    public class Pool : IDisposable
    {
        private String _Name;
        private String _Database;
        private String _Username;
        private String _Password;
        private String _Host;
        private Int32 _Port;
        private String _Instance;
        private Int32 _Timeout;
        private Int32 _Expiration;
        private List<Connection> _Connections;
        private Int32 _ConnectionsMin;
        private Int32 _ConnectionsMax;
        private Thread _MaintenanceThread;

        public String Name { get => _Name;}
        public String Database { get => _Database; set => _Database = value; }
        public String Username { get => _Username; set => _Username = value; }
        public String Password { get => _Password; set => _Password = value; }
        public String Host { get => _Host; set => _Host = value; }
        public Int32 Port { get => _Port; set => _Port = value; }
        public String Instance { get => _Instance; set => _Instance = value; }
        public Int32 Timeout
        {
            get => _Timeout; set
            {
                _Timeout = value;
                if (this._Timeout < 1)
                {
                    _Timeout = 1;
                }
                else if (this._Timeout > 60)
                {
                    _Timeout = 60;
                }
            }
        }
        public Pool()
        {
            this._Name = Assembly.GetEntryAssembly().GetName().ToString();
            this._Connections = new List<Connection>();
            this.Config();
        }
        public Pool(System.String Name)
        {
            this._Name = Name;
            this._Connections = new List<Connection>();
            this.Config();
        }
        public Boolean Connected()
        {
            for (Int32 _ConnectionLoop = 0; _ConnectionLoop < this._Connections.Count; _ConnectionLoop++)
            {
                if (this._Connections[_ConnectionLoop].Active())
                {
                    return true;
                }
            }
            return false;
        }
        public Boolean Open()
        {
            Tools.Log.Information(MethodBase.GetCurrentMethod(), "Opening connections.");
            Connection _Connection = this.Connection();
            if (_Connection != null)
            {
                return true;
            }
            return false;
        }
        public Connection Connection()
        {
            Connection _Connection = null;
            lock (this._Connections)
            {
                if (this._Connections.Count < this._ConnectionsMin)
                {
                    for (Int32 _ConnectionLoop = this._Connections.Count; _ConnectionLoop < this._ConnectionsMin; _ConnectionLoop++)
                    {
                        _Connection = new Connection(this._Name)
                        {
                            _Host = this._Host,
                            _Port = this._Port,
                            _Timeout = this._Timeout,
                            _Database = this._Database,
                            _Username = this._Username,
                            _Password = this._Password,
                            _Instance = this._Instance,
                            _Expiration = this._Expiration,
                        };
                        if (_Connection.Open())
                        {
                            this._Connections.Add(_Connection);
                        }
                        else
                        {
                            _Connection = null;
                        }
                    }
                }
                if (this._Connections.Count == 0)
                {
                    return null;
                }
                try
                {
                    _Connection = this._Connections.FindLast(_connection => (_connection.Status == ConnectionStatus.Opened || _connection.Status == ConnectionStatus.StandBy));
                }
                catch { }
                if (_Connection == null)
                {
                    _Connection = new Connection(this._Name)
                    {
                        _Host = this._Host,
                        _Port = this._Port,
                        _Timeout = this._Timeout,
                        _Database = this._Database,
                        _Username = this._Username,
                        _Password = this._Password,
                        _Instance = this._Instance,
                        _Expiration = this._Expiration,
                        _Expires = ((this._Connections.Count >= this._ConnectionsMin) ? false : true)
                    };
                    if (_Connection.Open())
                    {
                        this._Connections.Add(_Connection);
                    }
                    else
                    {
                        _Connection = null;
                    }
                }
                if (this._Connections.Count >= this._ConnectionsMax)
                {
                    this.MaintenanceThreadCheckIn();
                }
            }
            return _Connection;
        }
        public Boolean Close()
        {
            Connection _Connection = null;
            Int32 _Connections = this._Connections.Count;
            Int32 _ConnectionsClosed = 0;
            for (Int32 _ConnectionLoop = 0; _ConnectionLoop < this._Connections.Count; _ConnectionLoop++)
            {
                _Connection = this._Connections[_ConnectionLoop];
                if (_Connection.Close())
                {
                    _ConnectionsClosed++;
                    this._Connections.RemoveAll(_finder => _finder.GUID == _Connection.GUID);
                }
            }
            return (_ConnectionsClosed == _Connections ? true : false);
        }
        public Command Command(String Query)
        {
            Connection _Connection = this.Connection();
            _Connection.Status = ConnectionStatus.Running;
            return new Command(Query, ref _Connection);
        }
        private void MaintenanceThread()
        {
            try
            {
                lock (this._Connections)
                {
                    List<Connection> _ConnectionsExpired = this._Connections.FindAll(_connection => (_connection.Expired == true));
                    if (_ConnectionsExpired != null && _ConnectionsExpired.Count > 0)
                    {
                        Tools.Log.Information(MethodBase.GetCurrentMethod(), "Cleaning up " + _ConnectionsExpired.Count.ToString() + " expired connections.");
                        foreach (Connection _ConnectionExpired in _ConnectionsExpired)
                        {
                            String _GUID = _ConnectionExpired.GUID;
                            Tools.Log.Information(MethodBase.GetCurrentMethod(), "Disposing connection " + _GUID + ".");
                            Tools.Log.Debug(MethodBase.GetCurrentMethod(), "-> Requests...: " + _ConnectionExpired.Usage.Requests + ".");
                            Tools.Log.Debug(MethodBase.GetCurrentMethod(), "-> Lifetime...: " + _ConnectionExpired.Usage.Lifetime + " (" + _ConnectionExpired.Usage.Seconds.ToString() + " seconds).");
                            Tools.Log.Debug(MethodBase.GetCurrentMethod(), "-> Created At.: " + _ConnectionExpired.Usage.CreatedAt.Datetime.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture) + ".");
                            Tools.Log.Debug(MethodBase.GetCurrentMethod(), "-> Last Usage.: " + _ConnectionExpired.Usage.LastUsage.Datetime.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture) + ".");
                            _ConnectionExpired.Dispose();
                            if (this._Connections.RemoveAll(_finder => _finder.GUID == _GUID) > 0)
                            {
                                Tools.Log.Information(MethodBase.GetCurrentMethod(), "Connection " + _GUID + " disposed successfully.");
                            }
                            else
                            {
                                Tools.Log.Information(MethodBase.GetCurrentMethod(), "Failed to dispose connection " + _GUID + ".");
                            }
                        }
                    }
                }
                
            }
            catch { }
        }
        private void MaintenanceThreadCheckIn()
        {
            try
            {
                Boolean _MaintenanceThreadStart = false;
                if (this._MaintenanceThread == null)
                {
                    _MaintenanceThreadStart = true;
                }
                else 
                {
                    switch (this._MaintenanceThread.ThreadState)
                    {
                        case ThreadState.Stopped:
                        case ThreadState.Aborted:
                        case ThreadState.Suspended:
                            _MaintenanceThreadStart = true;
                            break;
                    }
                }
                if (_MaintenanceThreadStart)
                {
                    this._MaintenanceThread = new Thread(new ThreadStart(this.MaintenanceThread))
                    {
                        Name = MethodBase.GetCurrentMethod().DeclaringType.Namespace + "." + MethodBase.GetCurrentMethod().DeclaringType.Name + ".MaintenanceThread",
                        Priority = ThreadPriority.Normal
                    };
                    this._MaintenanceThread.Start();
                }
            }
            catch { }
        }
        public void Dispose()
        {
            if (this._MaintenanceThread != null)
            {
                try
                {
                    this._MaintenanceThread.Abort();
                }
                catch { }
            }
            this.Close();
        }

        private void Config()
        {
            this._Host = Tools.INI.String(this._Name, "HOST", this._Host);
            this._Host = (this._Host == null ? Tools.INI.String("DATABASE", "HOST", null) : this._Host);

            this._Port = Tools.INI.Int32(this._Name, "PORT", this._Port);
            this._Port = (this._Port == 0 ? 1433 : this._Port);

            this._Database = Tools.INI.String(this._Name, "DATABASE", this._Database);
            this._Database = (this._Database == null ? Tools.INI.String("DATABASE", "DATABASE", null) : this._Database);

            this._Username = Tools.INI.String(this._Name, "USERNAME", this._Username);
            this._Username = (this._Username == null ? Tools.INI.String("DATABASE", "USERNAME", null) : this._Username);

            this._Password = Tools.INI.String(this._Name, "PASSWORD", this._Instance);
            this._Password = (this._Password == null ? Tools.INI.String("DATABASE", "PASSWORD", null) : this._Password);

            this._Instance = Tools.INI.String(this._Name, "INSTANCE", this._Instance);
            this._Instance = (this._Instance == null ? Tools.INI.String("DATABASE", "INSTANCE", null) : this._Instance);

            this._Timeout = Tools.INI.Int32(this._Name + @"/CONNECTION", "TIMEOUT", this._Timeout);
            this._Timeout = (this._Timeout == 0 ? Tools.INI.Int32("DATABASE" + @"/CONNECTION", "TIMEOUT", this._Timeout) : this._Timeout);
            this._Timeout = (this._Timeout < 5 ? 5 : this._Timeout);

            this._Expiration = Tools.INI.Int32(this._Name + @"/CONNECTION", "EXPIRATION", this._Expiration);
            this._Expiration = (this._Expiration == 0 ? Tools.INI.Int32("DATABASE" + @"/CONNECTION", "EXPIRATION", this._Expiration) : this._Expiration);
            this._Expiration = (this._Expiration < 600 ? 600 : this._Expiration);

            this._ConnectionsMin = Tools.INI.Int32(this._Name + @"/POOL", "MIN", this._ConnectionsMin);
            this._ConnectionsMin = (this._ConnectionsMin == 0 ? Tools.INI.Int32("DATABASE" + @"/POOL", "MIN", this._ConnectionsMin) : this._ConnectionsMin);
            this._ConnectionsMin = (this._ConnectionsMin < 1 ? 1 : this._ConnectionsMin);

            this._ConnectionsMax = Tools.INI.Int32(this._Name + @"/POOL", "MAX", this._ConnectionsMax);
            this._ConnectionsMax = (this._ConnectionsMax == 0 ? Tools.INI.Int32("DATABASE" + @"/POOL", "MAX", this._ConnectionsMax) : this._ConnectionsMax);
            this._ConnectionsMax = (this._ConnectionsMax < 32 ? 32 : this._ConnectionsMax);
        }
    }
}
