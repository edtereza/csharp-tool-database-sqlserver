﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools.Database.SQLServer
{
    public enum Interface_LogLevel
    {
        Critical = 0
        , Error = 1
        , Information = 2
        , Debug = 3
    }
    public class Interface_Log 
    {
        public String File
        {
            get
            {
                return Log.File;
            }
            set
            {
                Log.File = value;
            }
        }
        public String Path
        {
            get
            {
                return Log.Path;
            }
            set
            {
                Log.Path = value;
            }
        }
        public Interface_LogLevel Level
        {
            get
            {
                return (Interface_LogLevel)Log.Level;
            }
            set
            {
                Log.Level = (Log.LogLevel)value;
            }
        }
        public Boolean Enabled
        {
            get
            {
                return Log.Enabled;
            }
            set
            {
                Log.Enabled = value;
            }
        }
    }
}
