﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Tools.Database.SQLServer
{
    public enum ConnectionStatus
    {
        Closed = 0,
        Opened = 1,
        StandBy = 2,
        Running = 3,
        Busy = 3,
    }
    public class Connection : IDisposable
    {
        private Int32 _Commands;

        internal String _GUID;
        internal String _Name;
        internal String _Database;
        internal String _Username;
        internal String _Password;
        internal String _Host;
        internal Int32 _Port;
        internal String _Instance;
        internal Int32 _Timeout;
        internal Int32 _Expiration;
        internal Boolean _Expires;

        private ConnectionStatus _Status;

        private ConnectionUsage _Usage;

        private Thread _ReconnectThread;
        private Boolean _ReconnectRetry;

        internal SqlConnection _SqlConnection;

        public String GUID { get => _GUID; }
        public String Name { get => _Name; }
        public String Database { get => _Database; }
        public String Username { get => _Username; }
        public String Password { get => _Password; }
        public String Host { get => _Host; }
        public Int32 Port { get => _Port; }
        public String Instance { get => _Instance; }
        public Int32 Timeout { get => _Timeout; }
        public Int32 Expiration
        {
            get => _Expiration; set
            {
                _Expiration = value;
                if (this._Expiration < 60)
                {
                    _Expiration = 1;
                }
                else if (this._Expiration > 3600)
                {
                    _Expiration = 3600;
                }
            }
        }
        public ConnectionStatus Status { get => _Status; internal set => _Status = value; }
        public Boolean Expired
        {
            get
            {
                if ((this._Usage.LastUsage.Unixtime + (this._Expiration * 60)) < this.GetUnixtime())
                {
                    return true;
                }
                return false;
            }
        }
        public ConnectionUsage Usage { get => this._Usage;  }
        public Connection(String Name)
        {
            this._GUID = this.GenerateGUID();
            this._Name = Name;
            this._Status = ConnectionStatus.Closed;
            this._Usage = new ConnectionUsage();
            this._Commands = 0;
        }
        public Boolean Active()
        {
            this._Usage.CheckIn();
            if (this._SqlConnection is SqlConnection && this._SqlConnection.State == ConnectionState.Open)
            {
                return true;
            }
            else
            {
                Boolean _BLN_ReconnectThread = false;
                if (this._ReconnectThread == null)
                {
                    _BLN_ReconnectThread = true;
                }
                else
                {
                    switch (this._ReconnectThread.ThreadState)
                    {
                        case ThreadState.Aborted:
                        case ThreadState.Stopped:
                        case ThreadState.Unstarted:
                            _BLN_ReconnectThread = true;
                            break;
                    }
                }
                if (_BLN_ReconnectThread)
                {
                    this._ReconnectThread = new Thread(new ThreadStart(this.ReconnectThread))
                    {
                        Name = this._GUID,
                        Priority = ThreadPriority.AboveNormal
                    };
                    this._ReconnectThread.Start();
                }                
            }
            return false;
        }
        public Boolean Open()
        {
            this._Usage.CheckIn();
            if (this._SqlConnection == null || ! this.Active())
            {
                this._Host = (this._Host == null ? "" : this._Host);
                this._Port = (this._Instance == null ? this._Port : 1433);
                this._Username = (this._Username == null ? "" : this._Username);
                this._Password = (this._Password == null ? "" : this._Password);
                this._Database = (this._Database == null ? "" : this._Database);
                try
                {
                    if (this._Instance != null && this._Instance.Length > 0)
                    {
                        Log.Information(MethodBase.GetCurrentMethod(), "Opening connection '" + this._GUID + "' to '" + this._Host + "' on instance '" + this._Instance + "'.");
                    }
                    else
                    {
                        Log.Information(MethodBase.GetCurrentMethod(), "Opening connection '" + this._GUID + "' to '" + this._Host + "' on port '" + this._Port.ToString() + "'.");
                    }
                    Log.Debug(MethodBase.GetCurrentMethod(), "-> Timeout.: " + this._Timeout.ToString());
                    Log.Debug(MethodBase.GetCurrentMethod(), "-> Hostname.: " + this._Host);
                    Log.Debug(MethodBase.GetCurrentMethod(), "-> Username.: " + this._Username);
                    Log.Debug(MethodBase.GetCurrentMethod(), "-> Password.: " + this._Password);
                    Log.Debug(MethodBase.GetCurrentMethod(), "-> Database.: " + this._Database);
                    if (this.Instance != null)
                    { 
                        Log.Debug(MethodBase.GetCurrentMethod(), "-> Instance.: " + this._Instance);
                    }
                }
                catch { }
                this._SqlConnection = new SqlConnection("user id=" + this._Username + ";" +
                                                "password=" + this._Password + ";" +
                                                "server=" + this._Host + (this._Port != 1433 ? ":" + this._Port.ToString() : "") + ";" +
                                                "database=" + this._Database + "; " +
                                                (this._Instance != null && this._Instance.Length > 0 ? "instance=" + this._Instance + "; " : "") +
                                                "connection timeout=" + this._Timeout);
                try
                {
                    this._SqlConnection.Open();
                    if (this.Instance != null && this._Instance.Length > 0)
                    {
                        Log.Information(MethodBase.GetCurrentMethod(), "Connection '" + this._GUID + "' successfully opened to '" + this._Host + "' on instance '" + this._Instance + "'.");
                    }
                    else
                    {
                        Log.Information(MethodBase.GetCurrentMethod(), "Connection '" + this._GUID + "' successfully opened to '" + this._Host + "' on port '" + this._Port.ToString() + "'.");
                    }
                    this._Status = ConnectionStatus.Opened;
                    return true;
                }
                catch (InvalidOperationException _InvalidOperationException)
                {
                    if (this.Instance != null && this._Instance.Length > 0)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), "Failed to open connection '" + this._GUID + "' to '" + this._Host + "' on instance '" + this._Instance + "' due Invalid Operation Exception.");
                    }
                    else
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), "Failed to open connection '" + this._GUID + "' to '" + this._Host + "' on port '" + this._Port.ToString() + "' due Invalid Operation Exception.");
                    }
                    Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_InvalidOperationException);
                }
                catch (SqlException _SqlException)
                {
                    if (this.Instance != null && this._Instance.Length > 0)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), "Failed to open connection '" + this._GUID + "' to '" + this._Host + "' on instance '" + this._Instance + "' due SQL Exception.");
                    }
                    else
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), "Failed to open connection '" + this._GUID + "' to '" + this._Host + "' on port '" + this._Port.ToString() + "' due SQL Exception.");
                    }
                    Log.Error(MethodBase.GetCurrentMethod(), "-> Código...: " + _SqlException.ErrorCode.ToString());
                    Log.Error(MethodBase.GetCurrentMethod(), "-> Mensagem.: " + _SqlException.Message);
                    Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_SqlException);
                }
                catch (System.Configuration.ConfigurationException _ConfigurationException)
                {
                    if (this.Instance != null && this._Instance.Length > 0)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), "Failed to open connection '" + this._GUID + "' to '" + this._Host + "' on instance '" + this._Instance + "' due System Configuration Exception.");
                    }
                    else
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), "Failed to open connection '" + this._GUID + "' to '" + this._Host + "' on port '" + this._Port.ToString() + "' due System Configuration Exception.");
                    }
                    Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_ConfigurationException);
                }
                catch (Exception _Exception)
                {
                    if (this.Instance != null && this._Instance.Length > 0)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), "Failed to open connection '" + this._GUID + "' to '" + this._Host + "' on instance '" + this._Instance + "' due Unknow Exception.");
                    }
                    else
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), "Failed to open connection '" + this._GUID + "' to '" + this._Host + "' on port '" + this._Port.ToString() + "' due Unknow Exception.");
                    }
                    Log.Exception(MethodBase.GetCurrentMethod(), _Exception);
                }
            }
            return this.Active();
        }
        public Boolean Close()
        {
            if (this.Active())
            {
                if (this._ReconnectThread != null)
                {
                    switch (this._ReconnectThread.ThreadState)
                    {
                        case ThreadState.Running:
                            try
                            {
                                this._ReconnectThread.Interrupt();
                            }
                            catch { }
                            break;
                    }
                }
                if (this.Instance != null)
                {
                    Log.Information(MethodBase.GetCurrentMethod(), "Closing opened connection '" + this.GUID +"' to '" + this._Host + "' on instance '" + this._Instance + "'.");
                }
                else
                {
                    Log.Information(MethodBase.GetCurrentMethod(), "Closing opened connection '" + this.GUID + "'to '" + this._Host + "' on port '" + this._Port.ToString() + "'.");
                }
                try
                {
                    this._SqlConnection.Close();
                    this._SqlConnection.Dispose();
                    this._SqlConnection = null;
                    if (this.Instance != null)
                    {
                        Log.Information(MethodBase.GetCurrentMethod(), "Connection '" + this.GUID + "' successfully closed to '" + this._Host + "' on instance '" + this._Instance + "'.");
                    }
                    else
                    {
                        Log.Information(MethodBase.GetCurrentMethod(), "Connection '" + this.GUID + "' successfully closed to '" + this._Host + "' on port '" + this._Port.ToString() + "'.");
                    }
                }
                catch (SqlException _SqlException)
                {
                    if (this.Instance != null)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), "Failed to close connection '" + this.GUID + "' to '" + this._Host + "' on instance '" + this._Instance + "' due SQL Exception.");
                    }
                    else
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), "Failed to close connection '" + this.GUID + "' to '" + this._Host + "' on port '" + this._Port.ToString() + "' due SQL Exception.");
                    }
                    Log.Error(MethodBase.GetCurrentMethod(), "-> Error...: " + _SqlException.ErrorCode.ToString());
                    Log.Error(MethodBase.GetCurrentMethod(), "-> Message.: " + _SqlException.Message);
                    Log.Exception(MethodBase.GetCurrentMethod(), (Exception)_SqlException);
                }
                catch (Exception _Exception)
                {
                    if (this.Instance != null)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), "Failed to close connection '" + this.GUID + "' to '" + this._Host + "' on instance '" + this._Instance + "' due Unknow Exception.");
                    }
                    else
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), "Failed to close connection '" + this.GUID + "' to '" + this._Host + "' on port '" + this._Port.ToString() + "' due Unknow Exception.");
                    }
                    Log.Exception(MethodBase.GetCurrentMethod(), _Exception);
                }
            }
            return false;
        }
        public Boolean Check()
        {
            this._Usage.CheckIn();
            Boolean _Boolean = false;
            if (this.Active())
            {
                this._Status = ConnectionStatus.Running;
                SqlCommand _SqlCommand = new SqlCommand("select 1", this._SqlConnection);
                try
                {
                    _Boolean = (_SqlCommand.ExecuteNonQuery() > 0 ? true : false);
                }
                catch
                {
                    _Boolean = false;
                }
                finally
                {
                    try
                    {
                        _SqlCommand.Dispose();
                    }
                    catch { }
                    this._Status = ConnectionStatus.StandBy;
                }
            }
            return _Boolean;
        }
        public void Acquire()
        {
            this._Commands++;
            this.Status = ConnectionStatus.Busy;
            this.Usage.CheckIn();
        }
        public void Running()
        {
            this.Status = ConnectionStatus.Running;
            this.Usage.CheckIn();
        }
        public void Release()
        {
            this.Status = ConnectionStatus.StandBy;
            this.Usage.CheckIn();
        }
        public void Dispose()
        {
            this.Close();
        }
        private void ReconnectThread()
        {
            try
            {
                this._ReconnectRetry = true;
                while (this._ReconnectRetry)
                {
                    if (!this.Open())
                    {
                        if (this._ReconnectRetry)
                        {
                            Thread.Sleep((new Random()).Next(500, 1000));
                        }
                    }
                    else
                    {
                        this._ReconnectRetry = false;
                    }
                }
            }
            catch { }
        }
        private String GenerateGUID()
        {
            Guid _Guid = Guid.NewGuid();
            String _String = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture) + "::" + _Guid.ToString();
            MD5 _MD5 = System.Security.Cryptography.MD5.Create();
            byte[] _Bytes = System.Text.Encoding.ASCII.GetBytes(_String);
            byte[] _MD5Bytes = _MD5.ComputeHash(_Bytes);
            StringBuilder _StringBuilder = new StringBuilder();
            for (Int16 _MD5BytesLoop = 0; _MD5BytesLoop < _MD5Bytes.Length; _MD5BytesLoop++)
            {
                _StringBuilder.Append(_MD5Bytes[_MD5BytesLoop].ToString("X2"));
            }
            _String = _StringBuilder.ToString();
            return _String.Substring(0, 8) + "-" +
                _String.Substring(8, 4) + "-" +
                _String.Substring(12, 4) + "-" +
                _String.Substring(16, 4) + "-" +
                _String.Substring(20, 12);
        }
        private Int64 GetUnixtime()
        {
            return this.GetUnixtime(DateTime.Now);
        }
        private Int64 GetUnixtime(DateTime DateTime)
        {
            DateTime _DateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan _TimeSpan = DateTime - _DateTime;
            return (Int64)_TimeSpan.TotalSeconds;
        }
    }
}
