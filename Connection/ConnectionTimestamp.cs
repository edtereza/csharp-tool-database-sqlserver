﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools.Database.SQLServer
{
    public class ConnectionTimestamp : IDisposable
    {
        private Int64 _UnixTime;
        private DateTime _Datetime;
        public Int64 Unixtime { get => this._UnixTime; }
        public DateTime Datetime { get => this._Datetime; }
        public ConnectionTimestamp()
        {
            this.Update();
        }
        public void Update()
        {
            this._Datetime = DateTime.Now;
            this._UnixTime = (Int64)((this._Datetime) - (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))).TotalSeconds;
        }
        public void Update(DateTime DateTime)
        {
            this._Datetime = DateTime;
            this._UnixTime = (Int64)((DateTime) - (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))).TotalSeconds;
        }
        public void Dispose()
        {
            GC.Collect();
        }
    }
}
