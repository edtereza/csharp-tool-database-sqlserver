﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Tools.Database.SQLServer
{

    public class ConnectionUsage : IDisposable
    {
        private Int32 _Requests;
        private ConnectionTimestamp _CreatedAt;
        private ConnectionTimestamp _LastUsage;
        public Int32 Requests { get => this._Requests; }
        public ConnectionTimestamp CreatedAt { get => this._CreatedAt; }
        public ConnectionTimestamp LastUsage { get => this._LastUsage; }
        public String Lifetime
        {
            get
            {
                TimeSpan _TimeSpan = TimeSpan.FromSeconds((this._LastUsage.Unixtime - this._CreatedAt.Unixtime));
                DateTime _DateTime = DateTime.Today.Add(_TimeSpan);
                return _DateTime.ToString("hh:mm:tt");
            }
        }
        public Int64 Seconds
        {
            get
            {
                TimeSpan _TimeSpan = TimeSpan.FromSeconds((this._LastUsage.Unixtime - this._CreatedAt.Unixtime));
                return (Int64)_TimeSpan.TotalSeconds;
            }
        }
        public ConnectionUsage()
        {
            this._Requests = 0;
            this._CreatedAt = new ConnectionTimestamp();
            this._LastUsage = new ConnectionTimestamp();
        }
        public void CheckIn()
        {
            this._Requests++;
            this._LastUsage.Update();
        }
        public void Dispose()
        {
            GC.Collect();
        }
    }
}
